### Purpose

This workflow will change the names of the files in the input directory.

The new name is based on the input.txt, format in ```NewName-md5Hash.FileExtension```

### Input dir

- input.txt
    - each row contains: new name and current file path
    - for example: 
      ```
      us_covid_q4	input/W4-covid-19-questionnaire.pdf          
      ```

- files that need their name changed, for example ```W4-covid-19-questionnaire.pdf```


### Output artifacts

- files with name changed, for example ```us_covid_q4-d41d8cd98f00b204e9800998ecf8427e.pdf```
